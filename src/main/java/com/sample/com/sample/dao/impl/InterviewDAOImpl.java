package com.sample.com.sample.dao.impl;

import com.sample.com.sample.dao.InterviewDAO;
import com.sample.common.DatabaseHelper;
import com.sample.model.Record;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class InterviewDAOImpl implements InterviewDAO {
    final static Logger logger = Logger.getLogger(InterviewDAOImpl.class);


    @Override
    public void insert(Record record) {
        try (Connection connection = DatabaseHelper.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement("insert into records values(?,?,?,?,?,?,?,?,?,?)")) {
                statement.setString(1, record.getA());
                statement.setString(2, record.getB());
                statement.setString(3, record.getC());
                statement.setString(4, record.getD());
                statement.setString(5, record.getE());
                statement.setString(6, record.getF());
                statement.setBigDecimal(7, record.getG());
                statement.setBoolean(8, record.isH());
                statement.setBoolean(9, record.isI());
                statement.setString(10, record.getJ());
                statement.executeUpdate();
                logger.debug("Successfully inserted the file into the database!");
            } catch (SQLException e) {
                logger.error("error while closing statement", e);
            }
        } catch (SQLException e) {
            logger.error("error while closing connection", e);
        }
    }

}