--------------------------------------------------------------------------------
Requirement
--------------------------------------------------------------------------------
Parse data of a CSV file and Store the valid data in a database
Bad data in the csv file should be recorded in a seperate file
Write statistics about total number of received, successful, and failed records to a log file

--------------------------------------------------------------------------------
Implementation
--------------------------------------------------------------------------------
DatabaseHelper
	- establishes connection with the database
	- Settings related to the database, its schema and tables are specified here
	- Modify the settings according to the database environment being used
	
InterviewDAO 
	- An Interface which Contains one method (insert) declaration

InterviewDAOImpl 
	- Implements InterviewDAO
	- implements method insert which
		establishes connection with the database
		creates and executes SQL INSERT statement to insert the record that is passed as an argument

CSVBatch
	- implements method importCSV which
		takes the CSV file path as an argument
		using CSVReader reads the file and creates a list of records
		using CSVProcessor parse list of records, identifies the bad records and writes them to bad-data-<timestamp>.csv file
		using CSVWrites stores the parsed records in the database
		writes total number of received, successfully processed, and failed records to the log file
		
CSVReader
	- implements method readCSV which
		reads each line of the CSV file and saves them as records in a list
		returns the list of records
		
CSVProcessor
	- implements method processRecords which
		takes the list of records from CSVReader
		checks and if a record is valid using processEachRecord method, it parses the record and creates a Record object
		if a record is bad, writes it to bad-data-<timestamp>.csv file
		returns list of all the Record objects which are ready to be inserted into database

CSVWriter
	- implements method writeRecords which
		takes the list of Record objects from CSVProcessor
		sends each Record to InterviewDAOImpl to insert it into database

log4j.xml
	Used to configure where the logger messages are written or displayed
	Defines the location of the log file and the message level i.e., what type of messages needs to be written to the file or displayed on console
	
TestCSVBatch
	- junit class used to
		test database connection
		test importing data into database from CSV file
		
pom.xml
	- Project Object Model XML file contains information about project that is used by Maven
	- It contains information related to build and dependencies

--------------------------------------------------------------------------------
DDL File
--------------------------------------------------------------------------------
csvdatabase.sql
	To create a new database table to store the data
	Location: \DDL\
--------------------------------------------------------------------------------
Input File:
--------------------------------------------------------------------------------
	Input file is a csv file
		File Name: ms3Interview.csv
		File Location: \src\test\resources\input\

--------------------------------------------------------------------------------
Output Files Details:
--------------------------------------------------------------------------------
	2 output files
	one is a .csv file which stores all the bad records
		File Name: bad-data-20170523125337.csv (The timestamp is file creation time. A new file will be created everytime the application run)
	Another is a .log file which stores the statistics of total number of received, successfull, and failed records
		File Name: output.log (It gets updated everytime the application run)
	Both files are saved in the application directory (in this case inside the repository ms3codingchallenge/)
	
--------------------------------------------------------------------------------
STEPS TO TEST/RUN THE APPLICATION
--------------------------------------------------------------------------------
	Run junit class TestCSVBatch
		method testDatabaseConnection() tests for the database connection
		method testImportCSV() tests the application
	
NOTE: There is no main(String[] args) method in CSVBatch class. 
	  testImportCSV() method of junit class TestCSVBatch can be implemented as main(String[] args) in CSVBatch class and the application can be tested by executing CSVBatch class.
