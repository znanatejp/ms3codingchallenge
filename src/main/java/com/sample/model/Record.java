package com.sample.model;

import java.beans.Transient;
import java.math.BigDecimal;

public class Record {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private BigDecimal g;
    private boolean h;
    private boolean i;
    private String j;

    private boolean validRecord;

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public String getF() {
        return f;
    }

    public void setF(String f) {
        this.f = f;
    }

    public BigDecimal getG() {
        return g;
    }

    public void setG(BigDecimal g) {
        this.g = g;
    }

    public boolean isH() {
        return h;
    }

    public void setH(boolean h) {
        this.h = h;
    }

    public boolean isI() {
        return i;
    }

    public void setI(boolean i) {
        this.i = i;
    }

    public String getJ() {
        return j;
    }

    public void setJ(String j) {
        this.j = j;
    }

    @Transient
    public boolean isValidRecord() {
        return validRecord;
    }

    public void setValidRecord(boolean validRecord) {
        this.validRecord = validRecord;
    }
}


