package com.sample.csv;

import com.sample.model.Record;
import org.apache.log4j.Logger;

import java.util.List;

public class CSVBatch {

    final static Logger LOGGER = Logger.getLogger(CSVBatch.class);

    public void importCSV(String csvFile) {
        CSVReader reader = new CSVReader();
        List<String> csvRecords = reader.readCSV(csvFile);

        CSVProcessor processor = new CSVProcessor();
        List<Record> records = processor.processRecords(csvRecords);

        CSVWriter writer = new CSVWriter();
        writer.writeRecords(records);

        LOGGER.info("# of records received " + csvRecords.size());
        LOGGER.info("# of records successful " + records.size());
        int failedRecordsCount = csvRecords.size() - records.size();
        LOGGER.info("# of records failed " + failedRecordsCount);
    }
}
