package com.sample.common;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DatabaseHelper {
    final static Logger logger = Logger.getLogger(DatabaseHelper.class);
    private static String JDBC_CONNECTION_URL =
            "jdbc:mysql://localhost:3306/csvdatabase";

    public static Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection(JDBC_CONNECTION_URL, "root", "znani");
        } catch (ClassNotFoundException e) {
            logger.error("JDBC Driver error", e);
        } catch (SQLException e) {
            logger.error("error while creating connection", e);
        }
        return connection;
    }

}
