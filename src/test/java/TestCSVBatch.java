import com.sample.common.DatabaseHelper;
import com.sample.csv.CSVBatch;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.sql.Connection;

public class TestCSVBatch {

    @Test
    public void testDatabaseConnection() {
        Connection connection =  DatabaseHelper.getConnection();
        Assert.assertNotNull(connection);
    }


    @Test
    public void testImportCSV() {
        CSVBatch batch = new CSVBatch();
        batch.importCSV("input/ms3Interview.csv");
    }
}
