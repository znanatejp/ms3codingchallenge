package com.sample.csv;

import com.sample.model.Record;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class CSVProcessor {

    final static Logger LOGGER = Logger.getLogger(CSVProcessor.class);
    private static String fileName = "bad-data-*.csv";
    private static String header = "A,B,C,D,E,F,G,H,I,J";

    public List<Record> processRecords(List<String> csvRecords) {
        List<Record> records = Collections.EMPTY_LIST;
        String timestamp = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
        fileName = fileName.replace("*", timestamp);
        try (BufferedWriter buffer = new BufferedWriter(new FileWriter(fileName))) {
            if (csvRecords != null || !csvRecords.isEmpty()) {
                records = new ArrayList<>(csvRecords.size());
                buffer.write(header);
                for (String csvRecord : csvRecords) {
                    Record record = processEachRecord(csvRecord);
                    if (record != null) {
                        records.add(record);
                    } else {
                        writeBadRecords(csvRecord,buffer);
                    }
                }
            }
        } catch (IOException e) {
            LOGGER.error(e);
        }
        return records;
    }


    public Record processEachRecord(String csvLine) {

        String[] array = csvLine.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
        if (array.length == 10) {
            Record record = new Record();
            String element = array[0];
            if (!StringUtils.isEmpty(element)) {
                record.setA(element);
            } else {
                return null;
            }
            element = array[1];
            if (!StringUtils.isEmpty(element)) {
                record.setB(element);
            } else {
                return null;
            }
            element = array[2];
            if (!StringUtils.isEmpty(element)) {
                record.setC(element);
            } else {
                return null;
            }
            element = array[3];
            if (!StringUtils.isEmpty(element)) {
                record.setD(element);
            } else {
                return null;
            }
            element = array[4];
            if (!StringUtils.isEmpty(element)) {
                record.setE(element);
            } else {
                return null;
            }
            element = array[5];
            if (!StringUtils.isEmpty(element)) {
                record.setF(element);
            } else {
                return null;
            }
            element = array[6];

            if (!StringUtils.isEmpty(element)) {
                String amount = element.replace("$", "");
                record.setG(new BigDecimal(amount));
            } else {
                return null;
            }

            element = array[7];
            if (!StringUtils.isEmpty(element)) {
                if ("true".equalsIgnoreCase(element)) {
                    record.setH(true);
                }
            } else {
                return null;
            }
            element = array[8];
            if (!StringUtils.isEmpty(element)) {
                if ("true".equalsIgnoreCase(element)) {
                    record.setI(true);
                }
            } else {
                return null;
            }
            element = array[array.length - 1];
            if (!StringUtils.isEmpty(element)) {
                record.setJ(element);
            } else {
                return null;
            }
            return record;
        }
        return null;
    }

    public static void writeBadRecords(String content, BufferedWriter writer) throws IOException {
        writer.write("\n");
        writer.write(content);
    }
}
