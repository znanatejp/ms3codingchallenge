package com.sample.csv;

import com.sample.com.sample.dao.InterviewDAO;
import com.sample.com.sample.dao.impl.InterviewDAOImpl;
import com.sample.model.Record;

import java.util.List;

public class CSVWriter {

    public void writeRecords(List<Record> records) {
        InterviewDAO dao = new InterviewDAOImpl();
        if (records != null || !(records.isEmpty())) {
            for (Record record : records) {
                dao.insert(record);
            }
        }
    }
}
