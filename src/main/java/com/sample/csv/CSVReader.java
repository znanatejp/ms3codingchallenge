package com.sample.csv;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CSVReader {
    final static Logger logger = Logger.getLogger(CSVReader.class);


    public List<String> readCSV(String csvFile) {
        Scanner scanner = null;
        List<String> records = new ArrayList<>();
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource(csvFile).getFile());
            scanner = new Scanner(file);
            if (scanner.hasNext()) {
                // skip header line
                scanner.nextLine();
            }
            String line = null;
            while (scanner.hasNext()) {
                line = scanner.nextLine();
                //skipping header if it exists in the middle of csv file
                if(!line.equals("A,B,C,D,E,F,G,H,I,J"))
                {
                    records.add(line);
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            logger.error("error in reading csv file", e);
        }
        return records;
    }

}